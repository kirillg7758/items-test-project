import { IEnvironment } from "./interface";

export const environment: IEnvironment = {
  production: true,
  API_KEY: '',
  DB_URL: ''
};
