import { Injectable } from "@angular/core";

@Injectable({ providedIn: "root" })

export class LoaderService {
  private visible: boolean = false

  open() {
    this.visible = true
  }

  close() {
    this.visible = false
  }

  isLoading() {
    return this.visible
  }
}
