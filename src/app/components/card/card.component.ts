import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IEmployer } from "../form/interfaces";
import { CardsService } from "./cards.service";
import { LoaderService } from "../loader/loader.service";

@Component({
  selector: 'app-card[card]',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  @Input() card: IEmployer
  @Output() remove: EventEmitter<string> = new EventEmitter<string>()

  constructor(private service: CardsService, private loading: LoaderService) { }

  ngOnInit(): void {
  }

  removeEmployer(id: string) {
    this.loading.open()
    this.service.delete(id)
      .subscribe(() => {
        this.remove.emit(id)
      })
  }

}
